<?php

namespace TestBundle\Services;

use Symfony\Component\HttpKernel\Log\LoggerInterface;
use TestBundle\Entity\Orders;
use Doctrine\ORM\EntityManager;

/**
 * Class LengowService
 *
 * @package TestBundle\Services
 */
class LengowService
{

	/**
	 * LengowService constructor.
	 *
	 * @param LoggerInterface $logger
	 * @param $url_orders
	 */
	public function __construct(LoggerInterface $logger, $url_orders, EntityManager $entityManager)
	{
		$this->logger = $logger;
		$this->url_orders = $url_orders;
		$this->em = $entityManager;
	}

	/**
	 * @return string, this is a flow xml
	 */
	public function getOutflowXml()
	{
		$xmlFlow = file_get_contents($this->url_orders);
		return $xmlFlow;
	}

	/**
	 * @param $outflowXml
	 * @return bool
	 */
	public function traceLogger($outflowXml)
	{
		$result = false;
		if ($outflowXml)
		{
			$this->logger->info('The outflow is successfully found');
			$result = true;
		}
		else
		{
			$this->logger->error('An error occurred, impossible to get the outflow ...');
		}

		return $result;
	}

	/**
	 * Read outflow xml and create object order
	 *
	 * @param $outflowXml
	 */
	public function setOrders($outflowXml)
	{
		try
		{
			$file = new \SimpleXMLElement($outflowXml);

			//Orders look through
			foreach ($file->orders as $order)
			{
				//Items look through
				foreach ($order as $item)
				{
					$ordersEntity = new Orders();

					$ordersEntity->setMarketplace($item->marketplace);
					$ordersEntity->setIdFlux($item->idFlux);
					$ordersEntity->setOrderId($item->order_id);
					$ordersEntity->setOrderTax($item->order_tax);
					$ordersEntity->setOrderAmount($item->order_amount);
					$ordersEntity->setOrderPurchaseDate(new \DateTime($item->order_purchase_date));

					$this->setOrderInDatabase($ordersEntity);
				}
			}
		}
		catch (\Exception $ex)
		{
			$this->logger->error("Adding orders in database without successfully {message}", ['message' => $ex->getMessage()]);
		}

	}

	/**
	 * Adding order if not exist in database
	 *
	 * @param $ordersEntity
	 */
	public function setOrderInDatabase(Orders $ordersEntity)
	{
		$ordersRepository = $this->getOrder($ordersEntity->getOrderId());

		if(empty($ordersRepository)){
			$this->em->persist($ordersEntity);
			$this->em->flush();
			$this->logger->info("Adding orders in database successfully");
		}
	}

	/**
	 * Method to get all Orders
	 * @return array|\TestBundle\Entity\Orders[]
	 */
	public function getOrders(){
		return $this->em->getRepository('TestBundle:Orders')->findAll();
	}

	/**
	 * Method to get Order with id param
	 * @param $id
	 * @return array|\TestBundle\Entity\Orders[]
	 */
	public function getOrder($id){
		return  $this->em->getRepository('TestBundle:Orders')->findBy(['orderId' => $id]);
	}
}
