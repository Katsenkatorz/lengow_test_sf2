<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Yaml\Dumper;
use Symfony\Component\HttpFoundation\Response;


class ApiController extends Controller
{
	public function indexAction()
	{
		return $this->render("TestBundle:Api:index.html.twig");
	}

	/**
	 * @return Response json
	 */
	public function ordersAction($format=null)
	{

		$lengowService = $this->container->get('lengow_test');
		$orders        = $lengowService->getOrders();
		$resultOrders  = $this->encode($orders, $format);

		return new Response($resultOrders);
	}

	/**
	 *
	 * @return Response json
	 */
	public function orderAction($id, $format=null)
	{

		$lengowService = $this->container->get('lengow_test');
		$orders        = $lengowService->getOrder($id);
		$resultOrders  = $this->encode($orders, $format);

		return new Response($resultOrders);

	}

	/**
	 * Encode function to json or yaml
	 * @param $orders
	 * @param $format
	 * @return string|\Symfony\Component\Serializer\Encoder\scalar
	 */
	private function encode($orders, $format)
	{
		switch ($format)
		{
			case "exempleResultatJson":
				$serializer = $this->get('serializer');
				$result     = $serializer->serialize($orders, 'json');
				break;
			case "exempleResultatYaml":
				$dumper = new Dumper();
				$result = $dumper->dump($orders,0,1,false,true);

				break;
			default:
				$result = "Le format choisi ne correspond pas, vous pouvez choisir yaml ou json."
					. "<br> par exemple :<br>- /api/exempleResultatJson.json"
					. "<br>- /api/exempleResultatYaml.yml";
				break;
		}

		return $result;
	}

}
