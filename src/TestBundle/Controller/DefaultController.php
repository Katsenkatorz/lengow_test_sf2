<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use APY\DataGridBundle\Grid\Source\Entity;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use TestBundle\Entity\Orders;
use TestBundle\Form\OrdersType;

class DefaultController extends Controller
{

	/**
	 * Grid action
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function gridAction()
	{
		$lengowService = $this->container->get('lengow_test');

		$outflowXml = $lengowService->getOutflowXml();
		//Adding orders to database if outflow
		if ($lengowService->traceLogger($outflowXml)) $lengowService->setOrders($outflowXml);

		$entity = new Orders();
		$form   = $this->createCreateForm($entity);

		// Creates simple grid based on your entity (ORM)
		$source = new Entity('TestBundle:Orders');

		// Get a grid instance
		$grid = $this->get('grid');

		// Attach the source to the grid
		$grid->setSource($source);

		// Manage the grid redirection, exports and the response of the controller
		return $grid->getGridResponse('TestBundle:ApyGrid:grid.html.twig', ['entity' => $entity, 'form' => $form->createView(),]);
	}

	/**
	 * Creates a form to create a Orders entity.
	 *
	 * @param Orders $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createCreateForm(Orders $entity)
	{
		$form = $this->createForm(new OrdersType(), $entity, ['action' => $this->generateUrl('orders_create'), 'method' => 'POST',]);

		$form->add('submit', 'submit', ['label' => 'Create']);

		return $form;
	}

	/**
	 * Creates a new Orders entity.
	 *
	 */
	public function createAction(Request $request)
	{
		$entity = new Orders();
		$form   = $this->createCreateForm($entity);
		$form->handleRequest($request);

		if ($form->isValid())
		{
			$lengowService =$this->container->get('lengow_test');
			$lengowService->setOrderInDatabase($entity);

			return $this->redirect($this->generateUrl('test_homepage', ['id' => $entity->getId()]));
		}

		return $this->myGridAction($entity,$form);
	}

}
